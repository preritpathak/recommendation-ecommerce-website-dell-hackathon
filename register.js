new Vue({
    el: "#register-container",
    data: {
      name: "",
      password: "",
      repassword: "",
      username: "",
      email: "",
      pno: "",
      errormsg: "",
      message: "",
      disabled: false
    },
    methods: {
      validateUser() {
        var self = this;
        self.disabled = true;
        if (this.phoneNos) {
          this.wpno = this.pno;
        }
        if (
          this.name == "" ||
          this.regno == "" ||
          this.password == "" ||
          this.repassword == "" ||
          this.username == "" ||
          this.email == "" ||
          this.pno == "" ||
          this.wpno == ""
        ) {
          if (this.name == "") {
            self.errormsg = "Name is Required!";
          } else if (this.regno == "") {
            self.errormsg = "Registration Number is Required!";
          } else if (this.password == "") {
            self.errormsg = "Password is Required!";
          } else if (this.repassword == "") {
            self.errormsg = "Password again is Required!";
          } else if (this.username == "") {
            self.errormsg = "Username is Required!";
          } else if (this.email == "") {
            self.errormsg = "Email is Required!";
          } else if (this.pno == "") {
            self.errormsg = "Phone Number is Required!";
          } else {
            self.errormsg = "Whatsapp Number is Required!";
          }
          return false;
        }
        if (this.password != this.repassword) {
          self.errormsg = "Both passwords don't match.";
          return false;
        }
        return true;
      },
      registerUser() {
        var result = this.validateUser();
        var self = this;
        if (!result) {
          self.disabled = false;
          return;
        }
        firebase
          .firestore()
          .collection("users")
          .where("username", "==", self.username)
          .get()
          .then(
            function(querySnapshot) {
              if (querySnapshot.size > 0) {
                self.errormsg =
                  "Username already exists. Please try with another username.";
                self.disabled = false;
              } else {
                firebase
                  .firestore()
                  .collection("users")
                  .where("pno", "==", self.pno)
                  .get()
                  .then(function(query) {
                    if (query.size > 0) {
                      self.errormsg =
                        "This Phone Number already has an account associated with it. Please try again!";
                      self.disabled = false;
                    } else {
                      firebase
                        .auth()
                        .createUserWithEmailAndPassword(self.email, self.password)
                        .then(
                          function(user) {
                            firebase
                              .firestore()
                              .collection("users")
                              .doc(user.user.uid)
                              .set({
                                name: self.name,
                                username: self.username,
                                email: self.email,
                                pno: self.pno,
                              })
                              .then(
                                function() {
                                  ("In");
                                  var body = {
                                    email: self.email,
                                    message: self.message,
                                    name: self.name
                                  };
                                  // fetch("/mail/checkMail.php", {
                                  //   method: "POST",
                                  //   headers: {
                                  //     "Content-Type": "application/json"
                                  //   },
                                  //   body: JSON.stringify(body)
                                  // })
                                  //   .then(res => {
                                  //     return res.json();
                                  //   })
                                  //   .then(response => {
                                  //     if (response.code === 200) {
                                  //       self.errormsg = "We'll get back to you!";
                                  //     } else if (response.code === 405) {
                                  //       self.errormsg = "Fields cant be empty!";
                                  //     } else if (response.code === 406) {
                                  //       self.errormsg = "Invalid E-Mail";
                                  //     }
                                  //   });
                                      self.errormsg = "Successfully Registered!";
                                      if((self.email).includes("dell.com")){
                                        setTimeout(() => {
                                          window.location = "/analytics";
                                        }, 1500);
                                      }
                                      else{
                                      setTimeout(() => {
                                        window.location = "/home";
                                      }, 1500);
                                    }
                                    self.clear();

                                  },
                                function(error) {
                                  self.errormsg = error.message;
                                  self.disabled = false;
                                }
                              );
                          },
      
                        );
                    }
                  });
              }
            },
            function(error) {
              this.errormsg = error.message;
              self.disabled = false;
            }
          );
      },

      clear() {
        this.name = "";
        this.email = "";
        this.pno = "";
        this.password = "";
        this.username = "";
        this.repassword = "";
      }
    }
  });
