const express = require("express");
const router = express.Router();
const fs = require('fs')
const { requireSignin, isAuth, isAdmin } = require("../controllers/auth");

const { userById, read, update } = require("../controllers/user");

router.get("/secret/:userId", requireSignin, isAuth, isAdmin, (req, res) => {
    res.json({
        user: req.profile
    });
});

router.get('/user/:userId', requireSignin,isAuth,read)
router.put('/user/:userId', requireSignin,isAuth,update)

router.param("userId", userById);


router.post('/data',(req,res) => {
    console.log(req.body.data);


    var jsonContent = JSON.stringify(req.body.data);

    fs.writeFile("output.json", jsonContent, 'utf8', function (err) {
        if (err) {
            console.log("An error occured while writing JSON Object to File.");
            return console.log(err);
        }
        console.log("JSON file has been saved.");
        res.status(200).json({
            error:"400 error"
        })

        //run python 
    });
})

router.get('/recommendedProducts', (req,res) => {
try{
    let jsonData = JSON.parse(fs.readFileSync('product.json', 'utf-8'))
    console.log(jsonData)
        res.send({products:jsonData})
}catch(err){
    res.send({products:[]})
}
})

module.exports = router;
var processor=0;
