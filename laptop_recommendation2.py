#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  7 16:18:45 2019

@author: Harsh
"""
import pandas as pd
import json

laptops= pd.read_csv("laptop_data2.csv")

data=open('output.json').read()
data1=json.loads(data)
del data1["name"]
del data1["role"]
"""
data = '{"budget": 50000, "ram": 8, "processor": 7, "size": 35,"touch": 1, "battery": 3}'
data = json.loads(data)
"""

indexNames = laptops[ laptops["budget"] > int(data1.get("budget"))].index
laptops.drop(indexNames , inplace=True)

for key,val in data1.items():
    if laptops.shape[0] >5 and key != "budget":
        indexNames = laptops[ laptops[key] != val ].index
        laptops.drop(indexNames , inplace=True)

X =laptops.iloc[:,1:2]
X = X.to_dict(orient='records')

  

with open('product.json', 'w') as json_file:
    json.dump(X, json_file)



  
  
